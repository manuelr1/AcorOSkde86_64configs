#!/bin/bash
PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"

# Set the working folder variable
acorbuild="$(pwd)"


# Create the build folder, move into it removing stale mountpoints and files there.
[ -e build ] && [ ! -d build ] && rm -f build || [ ! -e build ] && mkdir build
cd build
umount $(mount | grep "${PWD}/chroot" | tac | cut -f3 -d" ") 2>/dev/null
for i in ./* ./.build ./cache/bootstrap ; do [ $i = ./cache ] && continue || rm -rf $i ; done


# Set of the structure to be used for the ISO and Live system.
# See /usr/lib/live/build/config for a full list of examples.
# Up above is the manual description of what options I used so far.

lb config noauto \
	--binary-images iso-hybrid \
	--mode debian \
	--architectures amd64 \
	--linux-flavours amd64 \
	--distribution bullseye \
	--archive-areas "main contrib non-free" \
	--mirror-bootstrap https://deb.debian.org/debian \
	--parent-mirror-bootstrap https://deb.debian.org/debian \
	--parent-mirror-chroot https://deb.debian.org/debian \
	--parent-mirror-chroot-security https://security.debian.org/debian-security  \
	--parent-mirror-binary https://deb.debian.org/debian \
	--parent-mirror-binary-security https://security.debian.org/debian-security  \
	--mirror-chroot https://deb.debian.org/debian \
	--mirror-chroot-security https://security.debian.org/debian-security \
	--updates true \
	--security true \
	--backports false \
	--cache true \
	--apt-recommends true \
	--iso-application AcorOS \
	--win32-loader false \
	--iso-preparer acoros-https://sourceforge.net/projects/acor-os/ \
	--iso-publisher acoros-https://sourceforge.net/projects/acor-os/ \
	--iso-volume AcorOS \
	--image-name "AcorOS" \
	--win32-loader false \
	--checksums sha512 \
	--zsync false \
     "${@}"


# Install the Kde Desktop 
mkdir -p $acorbuild/build/config/package-lists
echo plasma-desktop > $acorbuild/build/config/package-lists/desktop.list.chroot 

# Install software
echo "# Install software to the squashfs for calamares to unpack to the OS.
linux-headers-amd64
locales
nala
dkms
dbus-x11
ntp
deb-multimedia-keyring
acoros-keyring
xorg 
xserver-xorg 
xserver-xorg-input-synaptics 
xserver-xorg-input-all 
xserver-xorg-video-vmware 
xserver-xorg-video-all
w64codecs
ffmpeg 
sox 
twolame 
lame 
faad 
gstreamer1.0-plugins-good 
gstreamer1.0-plugins-ugly 
gstreamer1.0-plugins-bad 
gstreamer1.0-pulseaudio
unrar 
rar 
p7zip-full 
p7zip-rar 
zip 
unzip
ark 
pavucontrol-qt
pavucontrol-qt-l10n
alsa-utils
aptitude 
synaptic 
gparted 
apt-config-auto-update 
libelf-dev 
htop 
desktop-base 
acoros-translations 
acoroslocale 
python3-gi-cairo
acorosstick 
gvfs-backends 
samba 
cifs-utils
smbclient
winbind
gnome-packagekit 
iso-flag-png 
network-manager 
gufw
acoros-icons 
blue-papirus-icons 
brown-papirus-icons 
papirus-cyan-icons 
papirus-dark-grey 
papirus-icon-theme 
materia-gtk-theme 
materia-kde 
orchis-gtk-theme
cups 
system-config-printer 
plymouth
plymouth-themes
sddm
kde-config-sddm
acoros-kde-wallpapers 
acoros-kde-configs 
plasma-discover-backend-flatpak
plasma-discover-backend-snap 
konsole 
dolphin dolphin-plugins 
qtcurve
yakuake
bluedevil 
kamera 
kate 
kcalc 
kde-spectacle 
kdeconnect 
kmag 
kscreen 
okular 
okular-extra-backends 
partitionmanager 
kmenuedit 
plasma-browser-integration 
print-manager 
kfind 
kdenetwork
kde-style-breeze 
kde-style-oxygen-qt5 
kde-style-qtcurve-qt5 
gtk2-engines-qtcurve
kde-config-gtk-style
kde-config-gtk-style-preview 
kde-config-screenlocker 
kde-config-systemd
plasma-nm
gtk2-engines-oxygen 
gtk2-engines-qtcurve 
gtk2-engines 
gtk3-engines-breeze
bleachbit
dconf-editor
gimp 
gimp-data-extras 
skanlite 
digikam
neofetch 
timeshift
transmission-gtk 
thunderbird 
thunderbird-l10n-pt-br 
thunderbird-l10n-pt-pt 
thunderbird-l10n-de 
thunderbird-l10n-en-gb 
thunderbird-l10n-es-es 
thunderbird-l10n-fr 
thunderbird-l10n-it
firefox
firefox-l10n-de 
firefox-l10n-en 
firefox-l10n-es
firefox-l10n-it 
firefox-l10n-br 
firefox-l10n-pt
libreoffice 
libreoffice-kf5 
libreoffice-qt5
libreoffice-l10n-pt 
libreoffice-l10n-pt-br 
libreoffice-l10n-de 
libreoffice-l10n-en-gb 
libreoffice-l10n-es 
libreoffice-l10n-fr 
libreoffice-l10n-it 
printer-driver-cups-pdf
kmahjongg 
2048-qt 
ksudoku 
dreamchess
guvcview 
vlc 
calamares-settings-acoros-kde
calamares 
firmware-linux 
firmware-linux-free
firmware-linux-nonfree 
firmware-misc-nonfree 
firmware-realtek 
firmware-atheros 
firmware-bnx2 
firmware-bnx2x 
firmware-brcm80211 
firmware-intelwimax 
firmware-iwlwifi 
firmware-libertas 
firmware-netxen 
firmware-zd1211  
gdebi 
f2fs-tools
xfsprogs
xfsdump
grub-pc

" > $acorbuild/build/config/package-lists/packages.list.chroot 


# Packages to be stored in /pool but not installed in the OS .
echo "# These packages are available to the installer, for offline use. 
efibootmgr
grub-common
grub2-common
grub-efi
grub-efi-amd64
grub-efi-amd64-bin
grub-efi-amd64-signed
libefiboot1
libefivar1
mokutil
os-prober
shim-helpers-amd64-signed
shim-signed
shim-signed-common
shim-unsigned

" > $acorbuild/build/config/package-lists/installer.list.binary 


# Setup the chroot structure
mkdir -p $acorbuild/build/config/archives
mkdir -p $acorbuild/build/config/includes.binary
mkdir -p $acorbuild/build/config/hooks/live
mkdir -p $acorbuild/build/config/hooks/normal
mkdir -p $acorbuild/build/config/bootloaders
mkdir -p $acorbuild/build/config/includes.chroot/usr/share/applications
mkdir -p $acorbuild/build/config/includes.chroot/etc/live/config.conf.d
mkdir -p $acorbuild/build/config/includes.chroot/usr/share/distro-info
mkdir -p $acorbuild/build/config/includes.chroot//usr/share/python-apt/templates
mkdir -p $acorbuild/build/config/includes.chroot/etc/dpkg/origins
mkdir -p $acorbuild/build/config/includes.chroot/usr/bin
mkdir -p $acorbuild/build/config/includes.chroot/usr/local/bin
mkdir -p $acorbuild/build/config/includes.chroot/etc/sddm.conf.d
mkdir -p $acorbuild/build/config/includes.chroot/boot/grub
mkdir -p $acorbuild/build/config/includes.chroot/etc/default

# Copy Configs to the chroot
cp $acorbuild/acoruserconfig/* $acorbuild/build/config/includes.chroot/etc/live/config.conf.d
cp $acorbuild/acorapplication/* $acorbuild/build/config/includes.chroot/usr/share/applications
cp $acorbuild/acorhooks/live/* $acorbuild/build/config/hooks/live
cp $acorbuild/acorhooks/normal/* $acorbuild/build/config/hooks/normal
cp $acorbuild/acorsddm/* $acorbuild/build/config/includes.chroot/etc/sddm.conf.d 
cp $acorbuild/acorgrub/grub $acorbuild/build/config/includes.chroot/etc/default

cp -r $acorbuild/acorcsv/* $acorbuild/build/config/includes.chroot/usr/share/distro-info
cp -r $acorbuild/acorgrub/themes $acorbuild/build/config/includes.chroot/boot/grub


# Place files unique to Unstable builds here.
cp -r $acorbuild/acorunstable/bootloaders/* $acorbuild/build/config/includes.binary
cp -r $acorbuild/acorunstable/repos/* $acorbuild/build/config/archives

#symlinks chroot
ln -s Debian.info $acorbuild/build/config/includes.chroot/usr/share/python-apt/templates/Acoros.info
ln -s Debian.mirrors $acorbuild/build/config/includes.chroot/usr/share/python-apt/templates/Acoros.mirrors
ln -s debian.csv $acorbuild/build/config/includes.chroot/usr/share/distro-info/acoros.csv
ln -s acoros $acorbuild/build/config/includes.chroot/etc/dpkg/origins/default



# Build the ISO #
lb build  #--debug --verbose 


